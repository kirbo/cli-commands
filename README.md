# <customer_name> CLI Commands

## Installation
Run:
```shell
bash install.sh
source ~/.bashrc
```
Now you can globally use all the commands found in `commands/` folder.

## Updating / pulling latest changes
In case the `post-merge` git hook is not working, run:
```shell
yarn
bash update.sh
source ~/.bashrc
```
Now you should have all the latest changes in use.

## Examples
All the commands output the usage help without any arguments given, e.g.:
```shell
$ firewall-list
###############################################################
#
#  Usage:
#      firewall-list <stage>
#
#  Parameters:
#
#     Name          Required    Default value     Description
#
#     stage         Yes                           Stage: all (dev, tst, qa & prd), tst (dev & tst), prd (qa & prd)
#
#    e.g.:
#      firewall-list all
#
#      firewall-list tst
#
#      firewall-list prd
#
###############################################################
```

### get-jwt
Generates JWT token for all environments:
```shell
$ get-jwt all
Generating JWT token for environment: local
JWT Token: eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJBcnNrYSIsIm5Hae3ROYW1lIjoiUmFha2EiLCJlbXBsb3llZUlkIjoiMTMzOCIsInRpdGxlIjoiQlBPJUFzc2lzdGFudCIsImRlcGFydG1lbnRDb2RlIjoiMzkxMDc1NSIsImlzU2FsZXNBZG1pbiI6ZmFsc2UsInZlcmlmaWVkIjphRag4ga.ltZ1q0-cWTFQmzEXi5ihexsH4slj0wbj4usaZtkmBpc

Generating JWT token for environment: dev
JWT Token: eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJBcnNrYSIsIm5Hae3ROYW1lIjoiUmFha2EiLCJlbXBsb3llZUlkIjoiMTMzOCIsInRpdGxlIjoiQlBPJUFzc2lzdGFudCIsImRlcGFydG1lbnRDb2RlIjoiMzkxMDc1NSIsImlzU2FsZXNBZG1pbiI6ZmFsc2UsInZlcmlmaWVkIjphRag4ga.TFwmAg4XncWdfn_hyilGuJhg92mn84NDjKlc7Q2Cn_Y

Generating JWT token for environment: tst
JWT Token: eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJBcnNrYSIsIm5Hae3ROYW1lIjoiUmFha2EiLCJlbXBsb3llZUlkIjoiMTMzOCIsInRpdGxlIjoiQlBPJUFzc2lzdGFudCIsImRlcGFydG1lbnRDb2RlIjoiMzkxMDc1NSIsImlzU2FsZXNBZG1pbiI6ZmFsc2UsInZlcmlmaWVkIjphRag4ga.JpZxfbqnPlvH3hwQ2N6JCouuvWEdcXdlZj5Rbtyv-rI

Generating JWT token for environment: qa
JWT Token: eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJBcnNrYSIsIm5Hae3ROYW1lIjoiUmFha2EiLCJlbXBsb3llZUlkIjoiMTMzOCIsInRpdGxlIjoiQlBPJUFzc2lzdGFudCIsImRlcGFydG1lbnRDb2RlIjoiMzkxMDc1NSIsImlzU2FsZXNBZG1pbiI6ZmFsc2UsInZlcmlmaWVkIjphRag4ga.HdCNpAbytzF-rZoqZR2gp1biH-f5RyOeROLIjopRR3U

Generating JWT token for environment: prd
JWT Token: eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJBcnNrYSIsIm5Hae3ROYW1lIjoiUmFha2EiLCJlbXBsb3llZUlkIjoiMTMzOCIsInRpdGxlIjoiQlBPJUFzc2lzdGFudCIsImRlcGFydG1lbnRDb2RlIjoiMzkxMDc1NSIsImlzU2FsZXNBZG1pbiI6ZmFsc2UsInZlcmlmaWVkIjphRag4ga.f_yDhhyna4vXH2krDQcQ_lATF9NqXxufQh6I2p2_eHU
```

Generates JWT token for given environment:
```shell
$ get-jwt tst
Generating JWT token for environment: tst
JWT Token: eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdE5hbWUiOiJBcnNrYSIsIm5Hae3ROYW1lIjoiUmFha2EiLCJlbXBsb3llZUlkIjoiMTMzOCIsInRpdGxlIjoiQlBPJUFzc2lzdGFudCIsImRlcGFydG1lbnRDb2RlIjoiMzkxMDc1NSIsImlzU2FsZXNBZG1pbiI6ZmFsc2UsInZlcmlmaWVkIjphRag4ga.JpZxfbqnPlvH3hwQ2N6JCouuvWEdcXdlZj5Rbtyv-rI
```

### bastion-tunnel
Creates a SSH tunnel for given environments DocumentDB cluster:
```shell
$ bastion-tunnel prd
Starting bastion-tunnel in environment: prd
Bastion already running on prd!
Fetching Bastion IP  Done!
Ensuring that <censored> exists in ~/.ssh/known_hosts  Done!
Ensuring that Bastion accepts ssh connections  Done!
Fetching Markdown cluster address ... Done!
Creating a tunnel in prd from local port 27017 into markdownclusterprd.cluster-<censored>.eu-west-1.docdb.amazonaws.com:27017...PID 38277 Created!
To close tunnel (and stop bastion), press CTRL + C                                                    [18:56:15] Sessions open: 0 logins, 1 tunnels.
```

Creates a SSH tunnel for given environments Redis cluster:
```shell
$ bastion-tunnel tst redis
Starting bastion-tunnel in environment: tst
Bastion already running on tst!
Fetching Bastion IP  Done!
Ensuring that <censored> exists in ~/.ssh/known_hosts  Done!
Ensuring that Bastion accepts ssh connections  Done!
Fetching Redis cluster address ... Done!
Creating a tunnel in tst from local port 6379 into sto-el-<censored>.dg4mpi.0001.euw1.cache.amazonaws.com:6379...PID 38735 Created!
To close tunnel (and stop bastion), press CTRL + C                                     [18:56:15] Sessions open: 0 logins, 1 tunnels.
```

### bastion-stop
Force shutdown bastion in environment:
```shell
$ bastion-stop tst
Stopping Bastion in environment: tst
Fetching Bastion IP  Done!
Ensuring that <censored> exists in ~/.ssh/known_hosts  Done!
Current session count, logins: 0, tunnels: 0
Do you still want to stop Bastion? [ y / N ]: y
Stopping Bastion, this will take a while ... Done!
```

### bastion-login
SSH Login into bastion in given environment:
```shell
$ bastion-login qa
Logging into bastion in environment: qa
Launching Bastion on qa, this will take a while ... Done!
Fetching Bastion IP  Done!
Ensuring that <censored> exists in ~/.ssh/known_hosts ... Done!
Ensuring that Bastion accepts ssh connections  Done!
Welcome to Ubuntu 16.04.4 LTS (GNU/Linux 4.4.0-1100-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

122 packages can be updated.
1 update is a security update.


Last login: Wed Oct 30 13:38:22 2019 from <censored>
_____________________________________________________________________
WARNING! Your environment specifies an invalid locale.
 The unknown environment variables are:
   LC_CTYPE=UTF-8 LC_ALL=
 This can affect your user experience significantly, including the
 ability to manage packages. You may install the locales by running:

   sudo apt-get install language-pack-UTF-8
     or
   sudo locale-gen UTF-8

To see all available language packs, run:
   apt-cache search "^language-pack-[a-z][a-z]$"
To disable this message for all users, run:
   sudo touch /var/lib/cloud/instance/locale-check.skip
_____________________________________________________________________

ubuntu@ip-censored:~$
```

### firewall-list
List all authorized rules in firewall:
```shell
$ firewall-list all
Authorized firewall rules in dev & tst:
  Protocol: tcp, Port:  5432
      IP                     Description
      ----------------------------------
      <censored>/32          EC2
      <censored>/32          from Customer Network
      <censored>/32          Colleague - home office
      <censored>/32          Colleague - mobile
      <censored>/32          Kimmo VPN
      <censored>/32          Kimmo Home Office

  Protocol: tcp, Ports: 27000 - 27100
      IP                     Description
      ----------------------------------
      <censored>/32          Kirbo-MBP-2018

Authorized firewall rules in qa & prd:
  Protocol: tcp, Port:  5432
      IP                     Description
      ----------------------------------
      <censored>/32          ec2 instances
      <censored>/32          Customer Network
      <censored>/32          Customer Network
      <censored>/32          Colleague - home office
      <censored>/32          Colleague - mobile
      <censored>/32          Kimmo VPN
      <censored>/32          Kimmo Home Office

  Protocol: tcp, Ports: 27000 - 27100
      IP                     Description
      ----------------------------------
      <censored>/32          Kirbo-MBP-2018
```

### firewall-authorize
Add an authorize rule into firewall:
```shell
$ firewall-authorize all 5432 <censored>/32 tcp "Kimmo VPN"
Authorizing:
 STAGES:        dev, tst, qa & prd
 PORT           5432
 PROTOCOL:      tcp
 IP:            <censored>/32
 DESCRIPTION:   Kimmo VPN

Do you want to proceed? [ y / N ]: y
Authorizing tcp 5432 <censored>/32 Kimmo VPN in dev & tst
Authorizing tcp 5432 <censored>/32 Kimmo VPN in qa & prd
Done
```

### firewall-revoke
Revoke an authorize rule from firewall:
```shell
$ firewall-revoke all 5432 <censored>/32 tcp
Revoking:
 STAGES:        dev, tst, qa & prd
 PORT           5432
 PROTOCOL:      tcp
 IP:            <censored>/32

Do you want to proceed? [ y / N ]: y
Revoking tcp 5432 <censored>/32  in dev & tst
Revoking tcp 5432 <censored>/32  in qa & prd
Done
```

### use-node
NVM install given Node version and reinstall all global packages with the new Node version:
```shell
$ use-node 12
*** Versions before
  * node       : v10.19.0
  * yarn       : 1.22.0
  * npm        : 6.13.4
*** Installing new versions
Downloading and installing node v12.15.0...
Local cache found: $NVM_DIR/.cache/bin/node-v12.15.0-darwin-x64/node-v12.15.0-darwin-x64.tar.xz
Checksums match! Using existing downloaded archive $NVM_DIR/.cache/bin/node-v12.15.0-darwin-x64/node-v12.15.0-darwin-x64.tar.xz
Now using node v12.15.0 (npm v6.13.4)
Reinstalling global packages from v10.19.0...
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/serverless -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/serverless/bin/serverless.js
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/slss -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/serverless/bin/serverless.js
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/sls -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/serverless/bin/serverless.js
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/webpack-bundle-size-analyzer -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/webpack-bundle-size-analyzer/webpack-bundle-size-analyzer
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/yarn -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/yarn/bin/yarn.js
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/yarnpkg -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/yarn/bin/yarn.js

> serverless@1.63.0 postinstall /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/serverless
> node ./scripts/postinstall.js


   ┌───────────────────────────────────────────────────┐
   │                                                   │
   │   Serverless Framework successfully installed!    │
   │                                                   │
   │   To start your first project run 'serverless'.   │
   │                                                   │
   └───────────────────────────────────────────────────┘

+ yarn@1.22.0
+ node-waf@1.0.0
+ webpack-bundle-size-analyzer@3.1.0
+ serverless@1.63.0
added 584 packages from 363 contributors in 17.202s
Linking global packages from v10.19.0...
No linked global packages found...
default -> 12 (-> v12.15.0)
Now using node v12.15.0 (npm v6.13.4)
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/yarn -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/yarn/bin/yarn.js
/Users/kirbo/.nvm/versions/node/v12.15.0/bin/yarnpkg -> /Users/kirbo/.nvm/versions/node/v12.15.0/lib/node_modules/yarn/bin/yarn.js
+ yarn@1.22.0
updated 1 package in 0.15s
*** Versions after
  * node       : v12.15.0
  * yarn       : 1.22.0
  * npm        : 6.13.4

✔ All done
```


### docker-destroy-container
Stops and removes docker containers for given project.
```
$ docker-destroy-container all
*** Stopping all docker containers.
  * ec2_microservice_name.postgres ... Done
  * sm-lambdas.postgres ... Done
  * sm-lambdas.redis ... Done
  * sm-markdown.mongo ... Done
  * sm-markdown.postgres ... Done
  * sm-changes.postgres ... Done
```


### <customer_name>-commands
Lists all <customer_name> commands.
- Without argument, lists only command names.
- With ANY argument given, lists which arguments these commands expect to receive.
```
$ <customer_name>-commands true
*** List of all <customer_name> commands
  * bastion-login <stage>
  * bastion-stop <stage>
  * bastion-tunnel <stage> [remote_host] [local_port] [remote_port]
  * docker-destroy-container <repo>
  * firewall-authorize <stage> <ports> [ip] [protocol] "[description for this rule]"
  * firewall-list <stage>
  * firewall-revoke <stage> <ports> <ip> [protocol]
  * get-jwt <stage>
  * use-node <version>

Hint:
  <argument> = Required argument
  [argument] = Optional argument
```
