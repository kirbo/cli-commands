#!/bin/bash

ALIASES_FILE="$(pwd)/.bash_aliases"
INSTALL_CLI_COMMANDS_PATH="$(pwd)"
CHANGES=false

if [[ "$(cat ~/.bashrc)" =~ "export PATH=\"${INSTALL_CLI_COMMANDS_PATH}/commands:\$PATH\"" ]]; then
  echo -n "Removing old \$PATH ~/.bashrc ..."
  OLD_PATH=$(echo "${INSTALL_CLI_COMMANDS_PATH}/commands" | sed 's/\//\\\//g')
  sed -i "" "/export PATH=\"${OLD_PATH}:\$PATH\"/d" ~/.bashrc
  echo " Done"
  CHANGES=true
fi

if [[ "$(cat ~/.bashrc)" =~ "source ${INSTALL_CLI_COMMANDS_PATH}/.bash_aliases" ]]; then
  echo -n "Removing old aliases file from ~/.bashrc ..."
  OLD_PATH=$(echo "${INSTALL_CLI_COMMANDS_PATH}" | sed 's/\//\\\//g')
  sed -i "" "/source ${OLD_PATH}\/.bash_aliases/d" ~/.bashrc
  echo " Done"
  CHANGES=true
fi

if [[ -z "${CLI_COMMANDS_PATH}" ]] && ! [[ "$(cat ~/.bashrc)" =~ "CLI_COMMANDS_PATH" ]]; then
  echo -n "Adding CLI_COMMANDS_PATH file into ~/.bashrc ..."
  echo "export CLI_COMMANDS_PATH='${INSTALL_CLI_COMMANDS_PATH}'" >> ~/.bashrc
  echo " Done"
  CHANGES=true
fi

if ! [[ "$(cat ~/.bashrc)" =~ "export PATH=\"\${CLI_COMMANDS_PATH}/commands:\$PATH\"" ]]; then
  echo -n "Adding CLI_COMMANDS_PATH into \$PATH in ~/.bashrc ..."
  echo "export PATH=\"\${CLI_COMMANDS_PATH}/commands:\$PATH\"" >> ~/.bashrc
  echo " Done"
  CHANGES=true
fi

if ! [[ "$(cat ~/.bashrc)" =~ "source \${CLI_COMMANDS_PATH}/.bash_aliases" ]]; then
  echo -n "Adding aliases file into ~/.bashrc ..."
  echo "source \${CLI_COMMANDS_PATH}/.bash_aliases" >> ~/.bashrc
  echo " Done"
  CHANGES=true
fi

# This should always be the last one.
# If you need to add things here, do it before this IF!
if [ "${CHANGES}" = true ]; then
  echo "New changes in ~/.bashrc. To get the changes in use, please run:"
  echo "  source ~/.bashrc"
  echo
  echo "Or close and open your terminal window."
fi
