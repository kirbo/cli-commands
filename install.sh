#!/bin/bash

INSTALL_CLI_COMMANDS_PATH="$(pwd)"
SCRIPTS_PATH="$(pwd)/commands"
ALIASES_FILE="$(pwd)/.bash_aliases"
YARN_INSTALLED=$(which yarn)
NPM_INSTALLED=$(which npm)
PACKAGES_INSTALLED=false
INSTALLED=false
LOADED=false

if [[ -z "${YARN_INSTALLED}" ]] && [[ -z "${NPM_INSTALLED}" ]]; then
  echo "Couldn't locate 'yarn' or 'npm'. Please install either one of these first."
  exit 1
fi

if ! [[ -z "${YARN_INSTALLED}" ]]; then
  echo "Yarn located, installing packages using yarn"
  yarn install
  PACKAGES_INSTALLED=true
else
  echo "Couldn't locate Yarn, but found npm. Installing Yarn."
  npm install -g yarn
  echo "Installing packages using yarn"
  yarn install
  PACKAGES_INSTALLED=true
fi


if [[ "${PATH}" =~ "${SCRIPTS_PATH}" ]]; then
  echo "Found ${SCRIPTS_PATH} from \$PATH"
  INSTALLED=true
  LOADED=true
else
  echo "Couldn't find ${SCRIPTS_PATH} from \$PATH"
fi

if [ "${INSTALLED}" = false ] && [[ "$(cat ~/.bashrc)" =~ "${SCRIPTS_PATH}" ]]; then
  echo "Found ${SCRIPTS_PATH} from ~/.bashrc"
  INSTALLED=true
else
  echo "Couldn't find ${SCRIPTS_PATH} from ~/.bashrc"
fi


if [ "${INSTALLED}" = false ]; then
  echo -n "Adding ${SCRIPTS_PATH} into ~/.bashrc ..."
  echo "export PATH=\"${SCRIPTS_PATH}:\$PATH\"" >> ~/.bashrc
  echo " Done"
else
  echo "Already installed."
fi

if [[ -z "${CLI_COMMANDS_PATH}" ]]; then
  echo -n "Adding CLI_COMMANDS_PATH file into ~/.bashrc ..."
  echo "export CLI_COMMANDS_PATH='${INSTALL_CLI_COMMANDS_PATH}'" >> ~/.bashrc
  echo " Done"
fi

if ! [[ "$(cat ~/.bashrc)" =~ "${ALIASES_FILE}" ]]; then
  echo -n "Adding aliases file into ~/.bashrc ..."
  echo "source ${ALIASES_FILE}" >> ~/.bashrc
  echo " Done"
else
  echo "Aliases already installed."
fi

# This should always be the last one.
# If you need to add things here, do it before this IF!
if [ "${LOADED}" = false ]; then
  echo "New \$PATH is not yet loaded. Please run:"
  echo "  source ~/.bashrc"
  echo
  echo "Or close and open your terminal window."
fi
