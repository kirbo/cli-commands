# Example of using `firewall-list`, `firewall-authorize` and `firewall-revoke`

```
[19:03:43] Kirbo-MBP-2018:~ kirbo$ firewall-list all
Authorized firewall rules in dev & tst:
  Protocol: tcp, Port:  5432
      IP                     Description
      ----------------------------------
      <censored>/32          EC2
      <censored>/32          from Customer Network
      <censored>/32          Colleague - home office
      <censored>/32          Colleague - mobile
      <censored>/32          Kimmo VPN
      <censored>/32          Kimmo Home Office

Authorized firewall rules in qa & prd:
  Protocol: tcp, Port:  5432
      IP                     Description
      ----------------------------------
      <censored>/32          ec2 instances
      <censored>/32          Customer Network
      <censored>/32          Customer Network
      <censored>/32          Colleague - home office
      <censored>/32          Colleague - mobile
      <censored>/32          Kimmo VPN
      <censored>/32          Kimmo Home Office

[19:03:50] Kirbo-MBP-2018:~ kirbo$ firewall-authorize all 27000-27100
Authorizing:
 STAGES:        dev, tst, qa & prd
 PORTS          27000 - 27100
 PROTOCOL:      tcp
 IP:            <censored>/32
 DESCRIPTION:   Kirbo-MBP-2018

Do you want to proceed? [ y / N ]: y
Authorizing tcp 27000-27100 <censored>/32 Kirbo-MBP-2018 in dev & tst
Authorizing tcp 27000-27100 <censored>/32 Kirbo-MBP-2018 in qa & prd
Done

[19:03:59] Kirbo-MBP-2018:~ kirbo$ firewall-list all
Authorized firewall rules in dev & tst:
  Protocol: tcp, Port:  5432
      IP                     Description
      ----------------------------------
      <censored>/32          EC2
      <censored>/32          from Customer Network
      <censored>/32          Colleague - home office
      <censored>/32          Colleague - mobile
      <censored>/32          Kimmo VPN
      <censored>/32          Kimmo Home Office

  Protocol: tcp, Ports: 27000 - 27100
      IP                     Description
      ----------------------------------
      <censored>/32        Kirbo-MBP-2018

Authorized firewall rules in qa & prd:
  Protocol: tcp, Port:  5432
      IP                     Description
      ----------------------------------
      <censored>/32          ec2 instances
      <censored>/32          Customer Network
      <censored>/32          Customer Network
      <censored>/32          Colleague - home office
      <censored>/32          Colleague - mobile
      <censored>/32          Kimmo VPN
      <censored>/32          Kimmo Home Office

  Protocol: tcp, Ports: 27000 - 27100
      IP                     Description
      ----------------------------------
      <censored>/32        Kirbo-MBP-2018

[19:04:03] Kirbo-MBP-2018:~ kirbo$ firewall-revoke all 27000-27100 <censored>/32
Revoking:
 STAGES:        dev, tst, qa & prd
 PORTS          27000 - 27100
 PROTOCOL:      tcp
 IP:            <censored>/32

Do you want to proceed? [ y / N ]: y
Revoking tcp 27000-27100 <censored>/32  in dev & tst
Revoking tcp 27000-27100 <censored>/32  in qa & prd
Done
```
