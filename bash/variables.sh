#!/bin/bash

### General variables
SSH_KEY="~/.ssh/bastion.pem"
BASTION_USER="ubuntu"

TIMEOUT=30
SESSION_COUNT=0
TERMINAL_WIDTH=$(tput cols)

TST_SECURITY_GROUP_ID=""
PRD_SECURITY_GROUP_ID=""

AWS_PROFILE="customer_name"

MARKDOWN_DUMP_PATH="/tmp/markdown/dump"
