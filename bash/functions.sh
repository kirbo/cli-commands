#!/bin/bash

# Function for outputting a "info" on the script
info () {
  echo -e "${BOLD}${@}${NORMAL}"
}

# Function for outputting a "step" on the script
step () {
  echo -e "${GREEN}***${DEFAULT}"$(info "${BOLD} ${@}${NORMAL}")
}

# Function for outputting a "minor step" on the script
minor_step () {
  echo -e "${CLEAR_LINE}${BOLD}  *${NORMAL} ${@}"
}

minor_step_updatable () {
  echo -ne "${CLEAR_LINE}${BOLD}  *${NORMAL} ${@}"
}

# Function to output a "divider"
output_divider() {
  TERMINAL_WIDTH="$(tput cols)"
  PADDING="$(printf '%0.1s' ={1..500})"
  DATE=$(date '+%Y-%m-%d %H:%M:%S')
  TEXT="${1} (${DATE})"
  printf '%*.*s %s %*.*s\n' 0 "$(((TERMINAL_WIDTH-2-${#TEXT})/2))" "$PADDING" "${TEXT}" 0 "$(((TERMINAL_WIDTH-1-${#TEXT})/2))" "$PADDING"
}

# Function for outputting a "all done" on the script
all_done () {
  echo -e "\n${GREEN}✔${NORMAL} ${BOLD}All done${NORMAL}"
}

# Function to check did the previous command succeed or not
continue_if_succeeded () {
  # Get the exit code of previous command
  RESULT=$?

  # If it did not exit with code 0 (succesful)
  if [ "$RESULT" != 0 ]; then
    # Run "hard_fail" function and pass the arguments
    hard_fail "Failed" $RESULT
  fi
}

# Output an error
step_failed () {
  # First argument is Message, by default it is "Failed"
  MESSAGE=${1:-"Failed"}
  # Second argument is the exit code, by default it is 1
  STATUS=${2:-1}

  # Define the error message prefix/suffix
  ERROR="${BLINK}${BOLD}${RED}!!!${NORMAL}"

  # Echo the error message with prefix & suffix and exit with given status code
  echo -e "${ERROR} ${MESSAGE} ${ERROR}"
}

# Output an error
hard_fail () {
  # First argument is Message, by default it is "Failed"
  MESSAGE=${1:-"Failed"}
  # Second argument is the exit code, by default it is 1
  STATUS=${2:-1}

  step_failed "${MESSAGE}" "${STATUS}"
  exit $STATUS
}

# Include NVM path
find_nvm () {
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
}


### General functions
processExit () {
  if [ "$1" == "tunnel" ]; then
    stopTunnel
  fi
  checkSessions
  echo "Current session count, logins: ${LOGIN_COUNT}, tunnels: ${TUNNEL_COUNT}"
  read -n 1 -t ${TIMEOUT} -p "Do you want to stop Bastion as well? [ y / N ]: " ANSWER
  if [ $? == 0 ]; then
    case $ANSWER in
      [Yy]* ) echo; checkStopBastion; exit;;
      * ) echo; exit;;
    esac
  else
    echo
    echo "No user input received in ${TIMEOUT} seconds!!!"
    checkStopBastion
  fi
}

pidExists () {
  if ! kill -0 $1 2>/dev/null; then
    return 1
  else
    return 0
  fi
}

checkTerminalWidth () {
  TERMINAL_WIDTH=$(tput cols)
}

hideCursor () {
  tput civis
}

showCursor () {
  tput cnorm
}

printLeftAndRight () {
  checkTerminalWidth
  LEFT=${1}
  RIGHT=${2}
  NEW_LINE=${3:-true}
  PREFIX="${CLEAR_LINE}"

  LINE=$(printf "%s%*s%s" "${LEFT}" $(expr ${TERMINAL_WIDTH} - ${#LEFT}) "${RIGHT}")

  if ${NEW_LINE}; then
    PREFIX=""
  fi

  echo -ne "${PREFIX}${LINE}"
}

### Bastion functions
checkSessions () {
  SESSIONS=$(ssh -i ${SSH_KEY} ${BASTION_USER}@${BASTION_IP} "ps -fp \$(pgrep -u ${BASTION_USER}) | grep \"[s]shd: ${BASTION_USER}\" | grep -v \"notty\"")
  LOGIN_COUNT=$(echo "${SESSIONS}" | grep -v -e '^[[:space:]]*$' | grep "\@pts" | wc -l | awk '{print $1}' || 0)
  TUNNEL_COUNT=$(echo "${SESSIONS}" | grep -v -e '^[[:space:]]*$' | grep -v "\@pts" | wc -l | awk '{print $1}' || 0)
  SESSION_COUNT=$(echo "${SESSIONS}" | grep -v -e '^[[:space:]]*$' | wc -l | awk '{print $1}' || 0)
}

checkBastionStatus () {
  fetchBastionId
  CURRENT_STATE=$(fetchCurrentState)
}

ensureBastionIsRunning () {
  checkBastionStatus
  if [ "${CURRENT_STATE}" != "running" ]; then
    STEP="Launching Bastion on ${STAGE}, this will take a while ... "
    echo -ne "${STEP}"
    START_INSTANCE=$(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws ec2 start-instances --instance-ids ${BASTION_ID} 2> /dev/null)
    ensureCurrentState "running" "${STEP}"
  else
    echo "Bastion already running on ${STAGE}!"
  fi
  ensureBastionIpAddress
  ensureSshdAcceptsConnections
}

checkIfAcceptsConnections () {
  ssh -i ${SSH_KEY} -o ConnectTimeout=1 -q ${BASTION_USER}@${BASTION_IP} "exit" >/dev/null 2>&1
  echo $?
}

ensureSshdAcceptsConnections () {
  echo -ne "Ensuring that Bastion accepts ssh connections "
  SSHD_ACCEPTS_CONNECTIONS=$(checkIfAcceptsConnections)
  while [ "${SSHD_ACCEPTS_CONNECTIONS}" != "0" ]; do
    sleep 1
    echo -ne "."
    SSHD_ACCEPTS_CONNECTIONS=$(checkIfAcceptsConnections)
  done
  echo " Done!"
}

checkStopBastion () {
  checkSessions
  if [ "${SESSION_COUNT}" -eq "0" ]; then
    stopBastion
  else
    echo "Current session count, logins: ${LOGIN_COUNT}, tunnels: ${TUNNEL_COUNT}, not stopping Bastion!"
  fi
  exit
}

stopBastion () {
  STEP="Stopping Bastion, this will take a while ... "
  echo -ne "${STEP}"
  CURRENT_STATE=$(fetchCurrentState)
  STOP_BASTION=$(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws ec2 stop-instances --instance-ids ${BASTION_ID} 2> /dev/null)
  ensureCurrentState "stopped" "${STEP}"
}

fetchCurrentState () {
  echo $(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws ec2 describe-instances --filters "Name=instance-id,Values=${BASTION_ID}" --query "Reservations[].Instances[].State.Name" --output text)
}

ensureCurrentState () {
  STATE=$1
  STEP=$2
  echo -ne "${CLEAR_LINE}${STEP}Current state: ${CURRENT_STATE}"
  while [ "${CURRENT_STATE}" != "${STATE}" ]; do
    sleep 1
    CURRENT_STATE=$(fetchCurrentState)
    echo -ne "${CLEAR_LINE}${STEP}Current state: ${CURRENT_STATE}"
  done
  CURRENT_STATE=$(fetchCurrentState)
  echo -ne "${CLEAR_LINE}${STEP}Done!"
  echo
}

askIfBastionRunning () {
  STAGE=$1
  echo -ne "Is '${GREEN}bastion-tunnel ${STAGE}${NORMAL}' running? [ y / N ]: "
  read -n 1 -t ${TIMEOUT} ANSWER
  if [ $? == 0 ]; then
    case $ANSWER in
    [Yy]* ) echo;;
    * ) echo; hard_fail "You must run '${GREEN}bastion-tunnel ${STAGE}${NORMAL}' in a separate terminal first.";;
  esac
  else
    echo
    hard_fail "No user input received in ${TIMEOUT} seconds"
  fi
}

### Markdown functions
fetchMarkdownCluster () {
  echo -n "Fetching Markdown cluster address ..."
  CLUSTER=$(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws docdb describe-db-clusters --region eu-west-1 --db-cluster-identifier "markdowncluster${STAGE}" --query 'DBClusters[*].[Endpoint,Port]' --output text)
  REMOTE_HOST=$(echo ${CLUSTER} | awk '{print $1}')
  if [ -z "$4" ]; then
    REMOTE_PORT=$(echo ${CLUSTER} | awk '{print $2}')
  fi
  if [ -z "$3" ]; then
    LOCAL_PORT=${REMOTE_PORT}
  fi
  echo " Done!"
}

### Redis functions
fetchRedisCluster () {
  echo -n "Fetching Redis cluster address ..."
  CLUSTER=$(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws elasticache describe-cache-clusters --region eu-west-1 --show-cache-node-info --query 'CacheClusters[*].CacheNodes[*].Endpoint.[Address,Port]' --output text)
  REMOTE_HOST=$(echo ${CLUSTER} | awk '{print $1}')
  if [ -z "$4" ]; then
    REMOTE_PORT=$(echo ${CLUSTER} | awk '{print $2}')
  fi
  if [ -z "$3" ]; then
    LOCAL_PORT=${REMOTE_PORT}
  fi
  echo " Done!"
}

### IP functions
fetchBastionId () {
  BASTION_ID=$(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws ec2 describe-instances --filters "Name=instance-type,Values=t2.micro" --query "Reservations[].Instances[].InstanceId" --output text)
}

fetchBastionIpAddress () {
  BASTION_IP=$(AWS_PROFILE=${AWS_PROFILE_PREFIX}-${STAGE} aws ec2 describe-instances --filters "Name=instance-id,Values=${BASTION_ID}" --query "Reservations[].Instances[].PublicIpAddress" --output text)
}

ensureBastionIpAddress () {
  echo -n "Fetching Bastion IP "
  fetchBastionIpAddress

  while [ -z "${BASTION_IP}" ]; do
    sleep 1
    fetchBastionIpAddress
    echo -n "."
  done
  echo " Done!"

  ensureKnownHost
}

ensureKnownHost () {
  echo -ne "Ensuring that ${BASTION_IP} exists in ~/.ssh/known_hosts "
  while [ -z "$(grep ${BASTION_IP} ~/.ssh/known_hosts)" ]; do
    sed -i "" "/# Bastion ${STAGE}/d" ~/.ssh/known_hosts
    echo "$(ssh-keyscan -t ecdsa ${BASTION_IP} 2>&1 | grep '^[^#]') # Bastion ${STAGE} $(date)" >> ~/.ssh/known_hosts
    echo -n "."
    sleep 1
  done
  echo " Done!"
}

### Tunnel functions
startTunnel () {
  TUNNEL_COMMAND="-i ${SSH_KEY} -L ${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} -N ${BASTION_USER}@${BASTION_IP}"
  TUNNEL_PID=$(ps -ax | grep "[s]sh ${TUNNEL_COMMAND}" | awk '{print $1}')
  if [ -z "${TUNNEL_PID}" ]; then
    echo -n "Creating a tunnel in ${STAGE} from local port ${LOCAL_PORT} into ${REMOTE_HOST}:${REMOTE_PORT}..."
    TUNNEL=$(ssh ${TUNNEL_COMMAND} & 2> /dev/null)
    TUNNEL_PID=$(ps -ax | grep "[s]sh ${TUNNEL_COMMAND}" | awk '{print $1}')
    echo "PID ${TUNNEL_PID} Created!"
  else
    echo "Using existing tunnel in ${STAGE} from local port ${LOCAL_PORT} into ${REMOTE_HOST}:${REMOTE_PORT} with PID ${TUNNEL_PID}!"
  fi

  RIGHT="[$(date +%T)] Checking open sessions..."
  LEFT="To close tunnel (and stop bastion), press CTRL + C"
  printLeftAndRight "${LEFT}" "${RIGHT}" false
  while $(pidExists "${TUNNEL_PID}") ; do
    checkSessions
    RIGHT="[$(date +%T)] Sessions open: ${LOGIN_COUNT} logins, ${TUNNEL_COUNT} tunnels."
    printLeftAndRight "${LEFT}" "${RIGHT}" false
    sleep 60
  done

  if ! $(pidExists "${TUNNEL_PID}"); then
    echo
    echo "Tunnel with PID ${TUNNEL_PID} has died!!"
    processExit "tunnel"
  fi
}

stopTunnel () {
  if $(pidExists "${TUNNEL_PID}"); then
    echo -n "Stopping tunnel ..."
    STOP_TUNNEL=$(kill -9 ${TUNNEL_PID} 2> /dev/null)
    while $(pidExists "${TUNNEL_PID}") ; do
      sleep .5
    done
    echo " Stopped!"
  fi
}
