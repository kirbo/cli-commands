#!/bin/bash

BASH_DIR="${CLI_COMMANDS_PATH}/bash"

source ${BASH_DIR}/colors.sh
source ${BASH_DIR}/variables.sh
source ${BASH_DIR}/functions.sh
