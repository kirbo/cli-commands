const { spawnSync } = require('child_process')

const AVAILABLE_STAGES = ['local', 'dev', 'tst', 'qa', 'prd']
let GET_FOR_STAGE = process.argv[2] || process.env.STAGE || undefined
let STAGES = AVAILABLE_STAGES

if (GET_FOR_STAGE !== undefined) {
  STAGES = [GET_FOR_STAGE]
}

const execute = () => {
  STAGES.forEach(async STAGE => {
    console.log('')
    spawnSync('node', [`${__dirname}/get-jwt.js`, STAGE], { stdio: 'inherit' });
  })
  console.log('')
}

execute()
