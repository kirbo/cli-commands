let STAGE = process.argv[2] || process.env.STAGE || 'tst'

process.env.AWS_PROFILE = !process.argv[2] && !process.env.STAGE && process.env.AWS_PROFILE ? process.env.AWS_PROFILE : `customer_name-${STAGE}`
process.env.AWS_SDK_LOAD_CONFIG = true

const SSM = require('aws-sdk/clients/ssm')
const ssm = new SSM()

STAGE = /customer_name-(.*)/.exec(process.env.AWS_PROFILE)[1]

const execute = async () => {
  const Name = process.argv[3]
  const fields = process.argv[4] || undefined
  const params = await ssm.getParameter({ Name, WithDecryption: true }).promise().then(p => JSON.parse(p.Parameter.Value))
  if (fields) {
    Object.keys(params).forEach(key => fields.includes(key) && console.log(`${key}=${params[key]}`))
  } else {
    Object.keys(params).forEach(key => console.log(`${key}=${params[key]}`))
  }
}

execute()
