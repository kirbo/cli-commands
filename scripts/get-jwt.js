const chalk = require('chalk');

let STAGE = process.argv[2] || process.env.STAGE || 'tst'

process.env.AWS_PROFILE = !process.argv[2] && !process.env.STAGE && process.env.AWS_PROFILE ? process.env.AWS_PROFILE : `customer_name-${STAGE}`
process.env.AWS_SDK_LOAD_CONFIG = true

const jwt = require('jsonwebtoken')
const SSM = require('aws-sdk/clients/ssm')
const ssm = new SSM()

STAGE = /ec2_microservice_name-(.*)/.exec(process.env.AWS_PROFILE)[1]

console.log('Generating JWT token for environment:', chalk.green(STAGE))

const execute = async () => {
  const secret = STAGE === 'local' ? 'reallysecretword' : await ssm.getParameter({ Name: `/ec2_microservice_name/${STAGE}/jwt_secret` }).promise().then(p => p.Parameter.Value)

  const payload = JSON.stringify({
    firstName: 'Arska',
    lastName: 'Raaka',
    employeeId: '1338',
    title: 'BPO%Assistant',
    departmentCode: '3910755',
    isSalesAdmin: false,
    verified: true
  })

  const token = await jwt.sign(payload, secret)

  console.log('JWT Token:', token)
}

execute()
