#!/bin/bash

printUsage() {
  echo -ne "${GREEN}"
 cat << EOF
###############################################################
#
#  Usage:
#      firewall-revoke <stage> <ports> <ip> [protocol]
#
#  Parameters:
#
#     Name          Required    Default value     Description
#
#     stage         Yes                           Stage to authorize. Values: all (dev, tst, qa & prd), tst (dev & tst), prd (qa & prd).
#     ports         Yes                           Either one port 27017 or a range 27000-27100.
#     ip            Yes                           IP address for the rule.
#     protocol      No          tcp               Protocol. By default it is "tcp".
#
#    e.g.:
#      firewall-revoke 27017 80.220.79.28
#
#      firewall-revoke 0-65535 80.220.79.28 tcp
#
#      firewall-revoke 21-22 80.220.79.28/32 tcp
#
###############################################################
EOF
echo -ne "${NORMAL}"
}

source ${CLI_COMMANDS_PATH}/bash/_includes.sh

STAGE=${1:-"help"}
PORTS=${2}
IP=${3}
PROTOCOL=${4:-"tcp"}

if [[ "${STAGE}" == "help" ]]; then
  printUsage
  exit
fi

if [[ -z "${PORTS}" ]] || [[ -z "${IP}" ]]; then
  printUsage
  exit 1
fi

if [[ "${PORTS}" =~ "-" ]]; then
  SPLIT_PORTS=$(echo "${PORTS}" | tr "-" " ")
  FROM_PORT=$(echo ${SPLIT_PORTS} | awk '{print $1}')
  TO_PORT=$(echo ${SPLIT_PORTS} | awk '{print $2}')
  PORT_TEXT="PORTS          ${GREEN}${FROM_PORT}${NORMAL} - ${GREEN}${TO_PORT}${NORMAL}"
else
  FROM_PORT="${PORTS}"
  TO_PORT="${PORTS}"
  PORT_TEXT="PORT           ${GREEN}${FROM_PORT}${NORMAL}"

fi


if ! [[ "${IP}" =~ "/" ]]; then
  IP="${IP}/32"
fi


revokeRule() {
  if [[ "${STAGE}" == "tst" ]] || [[ "${STAGE}" == "all" ]]; then
    echo -e "Revoking ${GREEN}${PROTOCOL} ${PORTS} ${IP} ${DESCRIPTION}${NORMAL} in ${GREEN}dev${NORMAL} & ${GREEN}tst${NORMAL}"
    AWS_PROFILE=${AWS_PROFILE_PREFIX}-tst aws ec2 revoke-security-group-ingress --group-id ${TST_SECURITY_GROUP_ID} --ip-permissions '[{"IpProtocol": "'${PROTOCOL}'", "FromPort": '${FROM_PORT}', "ToPort": '${TO_PORT}', "IpRanges": [{"CidrIp": "'${IP}'"}]}]'
  fi

  if [[ "${STAGE}" == "prd" ]] || [[ "${STAGE}" == "all" ]]; then
    echo -e "Revoking ${GREEN}${PROTOCOL} ${PORTS} ${IP} ${DESCRIPTION}${NORMAL} in ${GREEN}qa${NORMAL} & ${GREEN}prd${NORMAL}"
    AWS_PROFILE=${AWS_PROFILE_PREFIX}-prd aws ec2 revoke-security-group-ingress --group-id ${PRD_SECURITY_GROUP_ID} --ip-permissions '[{"IpProtocol": "'${PROTOCOL}'", "FromPort": '${FROM_PORT}', "ToPort": '${TO_PORT}', "IpRanges": [{"CidrIp": "'${IP}'"}]}]'
  fi

  echo "Done"
}

if [[ "${STAGE}" == "all" ]]; then
  STAGES="STAGES:        ${GREEN}dev${NORMAL}, ${GREEN}tst${NORMAL}, ${GREEN}qa${NORMAL} & ${GREEN}prd${NORMAL}"
elif [[ "${STAGE}" == "tst" ]]; then
  STAGES="STAGES:        ${GREEN}dev${NORMAL} & ${GREEN}tst${NORMAL}"
elif [[ "${STAGE}" == "prd" ]]; then
  STAGES="STAGES:        ${GREEN}qa${NORMAL} & ${GREEN}prd${NORMAL}"
fi

echo -e "Revoking:\n ${STAGES}\n ${PORT_TEXT}\n PROTOCOL:      ${GREEN}${PROTOCOL}${NORMAL}\n IP:            ${GREEN}${IP}${NORMAL}\n"
read -n 1 -t ${TIMEOUT} -p "Do you want to proceed? [ y / N ]: " ANSWER
if [ $? == 0 ]; then
    case $ANSWER in
      [Yy]* ) echo; revokeRule; exit;;
      * ) echo; exit;;
    esac
  else
    echo
    echo "No user input received in ${TIMEOUT} seconds!!!"
    echo "Not going to revoke."
fi
